export const environment = {
  production: true,
  apiBaseUrl: 'https://noroff-movie-catalogue.herokuapp.com/v1'
};
