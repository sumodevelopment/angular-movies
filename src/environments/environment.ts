export const environment = {
  production: false,
  apiBaseUrl: 'https://noroff-movie-catalogue.herokuapp.com/v1'
};
