import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from "src/environments/environment";
import { ApiResponse } from '../models/api-response.model';
import { Movie } from '../models/movie.model';
import { map } from 'rxjs/operators';


const { apiBaseUrl } = environment;

@Injectable({
	providedIn: 'root'
})
export class MoviesService {

	// Observable!
	private readonly _movies$: BehaviorSubject<Movie[]> = new BehaviorSubject([]);

	constructor(private readonly http: HttpClient) { }

	public movies$(): Observable<Movie[]> {
		return this._movies$.asObservable();
	}

	public fetchMovies(): void {
		this.http.get<ApiResponse>(`${apiBaseUrl}/movies`)
			.pipe(
				map((response: ApiResponse) => response.data)
			)
			.subscribe(
				(movies: Movie[]) => {
					this._movies$.next(movies);
				},
				(error) => {
					console.log('MoviesService.fetchMovies().error', error);
				}
			);
	}

}