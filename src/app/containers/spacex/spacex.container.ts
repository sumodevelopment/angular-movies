import { Component } from "@angular/core";

@Component({
	selector: 'app-spacex-container',
	template: `<h1>Space X</h1>`
})
export class SpacexContainer {}