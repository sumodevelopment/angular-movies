import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { Movie } from "src/app/models/movie.model";
import { MoviesService } from "src/app/services/movies.service";

@Component({
	selector: 'app-movies-container',
	templateUrl: './movies.container.html',
	styles: [
		`.movies {
			display: grid;
			grid-template-columns: repeat( auto-fill, minmax(200px, 1fr) );
			grid-column-gap: 1em;
			grid-row-gap: 1em;
		  }
		  `
	]
})
export class MoviesContainer {

	error: string = '';

	constructor(private readonly moviesService: MoviesService) { }

	// Component has loaded.
	ngOnInit(): void {
		this.moviesService.fetchMovies();
	}

	get movies$(): Observable<Movie[]> {
		return this.moviesService.movies$();
	}

	// // Event Handler for Output from MovieComponent
	// handleMovieClicked(movieId: number): void {
	// 	const movie = this.movies.find(m => m.id === movieId);
	// 	alert(`You clicked on ${movie.title}`);
	// }
}