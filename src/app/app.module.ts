import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MovieComponent } from './components/movie/movie.component';
import { ContainerComponent } from './components/container/container.component';
import { AppRoutingModule } from './app-routing.module';
import { MoviesContainer } from './containers/movie-container/movies.container';
import { SpacexContainer } from './containers/spacex/spacex.container';

@NgModule({
	declarations: [
		AppComponent,
		ContainerComponent,
		MoviesContainer,
		SpacexContainer,
		MovieComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		AppRoutingModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
