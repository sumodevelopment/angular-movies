export interface Movie {
	id?: string;
	title: string;
	description: string;
	cover: string;
	rating: number;
	genre: string[];
	director: string;
	cast: string[];
	active: number;
}