import {Component} from '@angular/core';

@Component({
  selector: 'app-container',
  // inline HTML in Angular - ng-content === React's props.children.
  template: `
    <div class="container">
      <ng-content></ng-content>
    </div>
  `,
  // Inline styles in Angular
  styles: [
    `.container {
      max-width: 1200px;
      width: 100%;
      margin: 0 auto;
      padding: 0 1em;
    }`
  ]
})
export class ContainerComponent {
}
