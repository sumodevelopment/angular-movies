import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { Movie } from "src/app/models/movie.model";
import { MoviesService } from "src/app/services/movies.service";

@Component({
	selector: 'app-footer',
	template: `
		<footer>
			Total movies: {{ movieCount$ | async }}
		</footer>
	`
})
export class AppFooter {
	constructor(private moviesService: MoviesService) {}

	get movieCount$(): Observable<Movie[]> {
		return this.moviesService.movies$()
	}
}