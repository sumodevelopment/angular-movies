import { Component, Input, EventEmitter, Output } from "@angular/core";

@Component({
	selector: 'app-movie',
	templateUrl: './movie.component.html',
  styleUrls: [ './movie.component.css' ]
})
export class MovieComponent {

	@Input() movie;
	@Output() clicked: EventEmitter<number> = new EventEmitter();


	onMovieClicked(): void {
		this.clicked.emit(this.movie.id)
	}



}
