import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MoviesContainer } from "./containers/movie-container/movies.container";
import { SpacexContainer } from "./containers/spacex/spacex.container";

const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: '/movies'
	},
	{
		path: 'movies',
		component: MoviesContainer
	},
	{
		path: 'space-x',
		component: SpacexContainer
	}
];

@NgModule({
	imports: [ RouterModule.forRoot( routes ) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule { }